package com.sma.yppkterunabakti.menu;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.fasterxml.jackson.databind.JsonNode;
import com.sma.yppkterunabakti.R;
import com.sma.yppkterunabakti.service.MainInterface;
import com.sma.yppkterunabakti.service.RestService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AbsensiActivity extends AppCompatActivity {

    private EditText tanggalText;
    private RecyclerView absensiView;
    private AbsensiAdapter absensiAdapter;
    private List<JsonNode> absensiList;
    private ProgressDialog progressDialog;
    private Calendar myCalendar = Calendar.getInstance();

    private DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absensi);

        tanggalText = (EditText) findViewById(R.id.txt_tanggal);
        absensiView = (RecyclerView) findViewById(R.id.recycler_absensi);
        absensiView.setHasFixedSize(false);
        absensiView.setLayoutManager(new LinearLayoutManager(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading data...");

        tanggalText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(AbsensiActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tanggalText.setText(sdf.format(myCalendar.getTime()));
        loadData();
    }

    private void loadData() {
        if (absensiList != null && !absensiList.isEmpty()) {
            absensiList.clear();
            absensiAdapter.notifyDataSetChanged();
        }
        progressDialog.show();
        Call<List<JsonNode>> result = RestService.createService(AbsensiActivity.this, MainInterface.class).getAbsensi(tanggalText.getText().toString());
        result.enqueue(new Callback<List<JsonNode>>() {
            @Override
            public void onResponse(Call<List<JsonNode>> call, Response<List<JsonNode>> response) {
                if (response.isSuccessful()) {
                    try {
                        if (absensiAdapter == null) {
                            absensiList = response.body();
                            absensiAdapter = new AbsensiAdapter(absensiList);
                            absensiAdapter.notifyDataSetChanged();
                            absensiView.setAdapter(absensiAdapter);
                        } else {
                            absensiList.clear();
                            absensiAdapter.notifyDataSetChanged();

                            absensiList.addAll(response.body());
                            absensiAdapter.notifyDataSetChanged();
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<JsonNode>> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public class AbsensiAdapter extends RecyclerView.Adapter<AbsensiHolder> {
        private List<JsonNode> absensi;

        public AbsensiAdapter(List<JsonNode> absensi) {
            this.absensi = absensi;
        }

        @Override
        public AbsensiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(AbsensiActivity.this).inflate(R.layout.list_absensi, parent, false);
            return new AbsensiHolder(view);
        }

        @Override
        public void onBindViewHolder(AbsensiHolder holder, int position) {
            JsonNode node = absensi.get(position);
            holder.bindSearch(node);
        }

        @Override
        public int getItemCount() {
            return absensi.size();
        }
    }

    public class AbsensiHolder extends RecyclerView.ViewHolder {
        private TextView mapelText, ketText;
        private JsonNode absensi;

        public AbsensiHolder(View itemView) {
            super(itemView);

            mapelText = (TextView) itemView.findViewById(R.id.txt_mapel);
            ketText = (TextView) itemView.findViewById(R.id.txt_keterangan);
        }

        public void bindSearch(JsonNode node) {
            absensi = node;

            mapelText.setText(getString(R.string.mapel) + " " + node.get("mapel_nama").asText());
            ketText.setText(getString(R.string.ket) + " " + node.get("absen_keterangan").asText());
        }
    }
}
