package com.sma.yppkterunabakti.menu;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.fasterxml.jackson.databind.JsonNode;
import com.sma.yppkterunabakti.R;
import com.sma.yppkterunabakti.service.MainInterface;
import com.sma.yppkterunabakti.service.RestService;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KeuanganActivity extends AppCompatActivity {

    private EditText bulanTahunText;
    private TextView sppText, totalText, terbayarText, tersisaText, ketText;
    private ProgressDialog progressDialog;
    private int month, year;
    private int MAX_YEAR = 2099;

    private DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            KeuanganActivity.this.month = month;
            KeuanganActivity.this.year = year;

            bulanTahunText.setText(month + " - " + year);
            loadData();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keuangan);

        bulanTahunText = (EditText) findViewById(R.id.txt_bulan_tahun);
        sppText = (TextView) findViewById(R.id.txt_spp);
        totalText = (TextView) findViewById(R.id.txt_total);
        terbayarText = (TextView) findViewById(R.id.txt_terbayar);
        tersisaText = (TextView) findViewById(R.id.txt_tersisa);
        ketText = (TextView) findViewById(R.id.txt_keterangan);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading data...");

        bulanTahunText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getLayoutInflater();
                Calendar cal = Calendar.getInstance();

                View dialog = inflater.inflate(R.layout.dialog_month_year_picker, null);
                final NumberPicker monthPicker = (NumberPicker) dialog.findViewById(R.id.picker_month);
                final NumberPicker yearPicker = (NumberPicker) dialog.findViewById(R.id.picker_year);

                monthPicker.setMinValue(1);
                monthPicker.setMaxValue(12);
                if (month == 0)
                    monthPicker.setValue(cal.get(Calendar.MONTH) + 1);
                else
                    monthPicker.setValue(month);

                int year = cal.get(Calendar.YEAR);
                yearPicker.setMinValue(year);
                yearPicker.setMaxValue(MAX_YEAR);
                if (KeuanganActivity.this.year == 0)
                    yearPicker.setValue(year);
                else
                    yearPicker.setValue(KeuanganActivity.this.year);

                AlertDialog.Builder builder = new AlertDialog.Builder(KeuanganActivity.this);
                builder.setView(dialog)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                date.onDateSet(null, yearPicker.getValue(), monthPicker.getValue(), 0);
                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                builder.show();
            }
        });
    }

    private void loadData() {
        String month;
        if (this.month < 10) {
            month = "0" + this.month;
        } else {
            month = String.valueOf(this.month);
        }

        sppText.setText(R.string.spp);
        totalText.setText(R.string.total);
        terbayarText.setText(R.string.terbayar);
        tersisaText.setText(R.string.tersisa);
        ketText.setText(R.string.keterangan);

        progressDialog.show();
        Call<JsonNode> result = RestService.createService(KeuanganActivity.this, MainInterface.class).getKeuangan(month, year);
        result.enqueue(new Callback<JsonNode>() {
            @Override
            public void onResponse(Call<JsonNode> call, Response<JsonNode> response) {
                if (response.isSuccessful()) {
                    try {
                        JsonNode node = response.body();

                        sppText.setText(getString(R.string.spp) + " " + node.get(0).get("keuangan_spp").asText());
                        totalText.setText(getString(R.string.total) + " " + node.get(0).get("keuangan_total").asText());
                        terbayarText.setText(getString(R.string.terbayar) + " " + node.get(0).get("keuangan_terbayar").asText());
                        tersisaText.setText(getString(R.string.tersisa) + " " + node.get(0).get("keuangan_sisa").asText());
                        ketText.setText(getString(R.string.keterangan) + " " + node.get(0).get("keuangan_keterangan").asText());
                    } catch (IllegalStateException | NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonNode> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
