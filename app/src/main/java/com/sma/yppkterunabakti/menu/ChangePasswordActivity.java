package com.sma.yppkterunabakti.menu;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sma.yppkterunabakti.Config;
import com.sma.yppkterunabakti.LoginActivity;
import com.sma.yppkterunabakti.R;
import com.sma.yppkterunabakti.service.MainInterface;
import com.sma.yppkterunabakti.service.RestService;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText newPassword, confirmPassword;
    private TextView errorText;
    private Button changeButton;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Authenticating...");

        newPassword = (EditText) findViewById(R.id.txt_new_password);
        confirmPassword = (EditText) findViewById(R.id.txt_confirm_password);
        errorText = (TextView) findViewById(R.id.txt_password_error);
        changeButton = (Button) findViewById(R.id.btn_change_password);

        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPassword.setError(null);
                confirmPassword.setError(null);

                String newPass = newPassword.getText().toString();
                String confirmPass = confirmPassword.getText().toString();

                boolean error = false;
                View focusView = null;

                //validate newpass
                if (TextUtils.isEmpty(newPass)) {
                    newPassword.setError(getString(R.string.error_field_required));
                    focusView = newPassword;
                    error = true;
                }

                //validate confirmpass
                if (TextUtils.isEmpty(confirmPass)) {
                    confirmPassword.setError(getString(R.string.error_field_required));
                    focusView = confirmPassword;
                    error = true;
                }

                if (!newPass.equals(confirmPass)) {
                    errorText.setVisibility(View.VISIBLE);
                    confirmPassword.setError(getString(R.string.error_field_required));
                    focusView = confirmPassword;
                    error = true;
                }

                if (error) {
                    focusView.requestFocus();
                } else {
                    submitNew(newPass);
                }
            }
        });
    }

    private void submitNew(String password) {
        progressDialog.show();
        Call<ResponseBody> result = RestService.createService(this, MainInterface.class).change(password);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        logout();
                        Toast.makeText(ChangePasswordActivity.this, res, Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void logout() {
        progressDialog.show();
        Call<ResponseBody> result = RestService.createService(this, MainInterface.class).logout();
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    SharedPreferences.Editor editPref = getSharedPreferences(Config.PREFERENCES, MODE_PRIVATE).edit();
                    editPref.remove("token");
                    editPref.remove("siswa_id");
                    editPref.apply();

                    Toast.makeText(getApplicationContext(), "Logout successful.", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
