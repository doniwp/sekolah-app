package com.sma.yppkterunabakti.menu;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.fasterxml.jackson.databind.JsonNode;
import com.sma.yppkterunabakti.R;
import com.sma.yppkterunabakti.service.MainInterface;
import com.sma.yppkterunabakti.service.RestService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PelanggaranActivity extends AppCompatActivity {

    private EditText tanggalText;
    private RecyclerView pelanggaranView;
    private PelanggaranAdapter pelanggaranAdapter;
    private List<JsonNode> pelanggaranList;
    private ProgressDialog progressDialog;
    private Calendar myCalendar = Calendar.getInstance();

    private DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelanggaran);

        tanggalText = (EditText) findViewById(R.id.txt_tanggal);
        pelanggaranView = (RecyclerView) findViewById(R.id.recycler_pelanggaran);
        pelanggaranView.setHasFixedSize(false);
        pelanggaranView.setLayoutManager(new LinearLayoutManager(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading data...");

        tanggalText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(PelanggaranActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        tanggalText.setText(sdf.format(myCalendar.getTime()));
        loadData();
    }

    private void loadData() {
        if (pelanggaranList != null && !pelanggaranList.isEmpty()) {
            pelanggaranList.clear();
            pelanggaranAdapter.notifyDataSetChanged();
        }
        progressDialog.show();
        Call<List<JsonNode>> result = RestService.createService(PelanggaranActivity.this, MainInterface.class).getPelanggaran(tanggalText.getText().toString());
        result.enqueue(new Callback<List<JsonNode>>() {
            @Override
            public void onResponse(Call<List<JsonNode>> call, Response<List<JsonNode>> response) {
                if (response.isSuccessful()) {
                    try {
                        if (pelanggaranAdapter == null) {
                            pelanggaranList = response.body();
                            pelanggaranAdapter = new PelanggaranAdapter(pelanggaranList);
                            pelanggaranAdapter.notifyDataSetChanged();
                            pelanggaranView.setAdapter(pelanggaranAdapter);
                        } else {
                            pelanggaranList.clear();
                            pelanggaranAdapter.notifyDataSetChanged();

                            pelanggaranList.addAll(response.body());
                            pelanggaranAdapter.notifyDataSetChanged();
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<JsonNode>> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public class PelanggaranAdapter extends RecyclerView.Adapter<PelanggaranHolder> {
        private List<JsonNode> pelanggaran;

        public PelanggaranAdapter(List<JsonNode> pelanggaran) {
            this.pelanggaran = pelanggaran;
        }

        @Override
        public PelanggaranHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(PelanggaranActivity.this).inflate(R.layout.list_pelanggaran, parent, false);
            return new PelanggaranHolder(view);
        }

        @Override
        public void onBindViewHolder(PelanggaranHolder holder, int position) {
            JsonNode node = pelanggaran.get(position);
            holder.bindSearch(node);
        }

        @Override
        public int getItemCount() {
            return pelanggaran.size();
        }
    }

    public class PelanggaranHolder extends RecyclerView.ViewHolder {
        private TextView ketText;
        private JsonNode pelanggaran;

        public PelanggaranHolder(View itemView) {
            super(itemView);

            ketText = (TextView) itemView.findViewById(R.id.txt_keterangan);
        }

        public void bindSearch(JsonNode node) {
            pelanggaran = node;

            ketText.setText(node.get("pelanggaran_nama").asText() + " - " + node.get("pelanggaran_kode").asText());
        }
    }
}
