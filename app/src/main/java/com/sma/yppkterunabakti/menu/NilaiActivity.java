package com.sma.yppkterunabakti.menu;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.fasterxml.jackson.databind.JsonNode;
import com.sma.yppkterunabakti.R;
import com.sma.yppkterunabakti.service.MainInterface;
import com.sma.yppkterunabakti.service.RestService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NilaiActivity extends AppCompatActivity {

    private Spinner kelasSpinner;
    private RecyclerView nilaiView;
    private ArrayList<String> kelasList;
    private ProgressDialog progressDialog;
    private JsonNode kelasNode;
    private List<JsonNode> nilaiNode;
    private NilaiAdapter nilaiAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nilai);

        kelasList = new ArrayList<>();

        kelasSpinner = (Spinner) findViewById(R.id.spinner_kelas);
        nilaiView = (RecyclerView) findViewById(R.id.recycler_nilai);
        nilaiView.setHasFixedSize(false);
        nilaiView.setLayoutManager(new LinearLayoutManager(this));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading data...");

        getKelasData();

        kelasSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getNilaiData(kelasNode.get(position).get("kelas_id").asText());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getKelasData() {
        progressDialog.show();
        Call<JsonNode> result = RestService.createService(NilaiActivity.this, MainInterface.class).getKelas();
        result.enqueue(new Callback<JsonNode>() {
            @Override
            public void onResponse(Call<JsonNode> call, Response<JsonNode> response) {
                if (response.isSuccessful()) {
                    try {
                        kelasNode = response.body();

                        for (int i = 0; i < kelasNode.size(); i++) {
                            kelasList.add(kelasNode.get(i).get("kelas_kode").asText());
                        }
                        kelasSpinner.setAdapter(new ArrayAdapter<>(NilaiActivity.this, android.R.layout.simple_spinner_dropdown_item, kelasList));
                    } catch (IllegalStateException | NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonNode> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void getNilaiData(String kelas) {
        if (nilaiNode != null && !nilaiNode.isEmpty()) {
            nilaiNode.clear();
            nilaiAdapter.notifyDataSetChanged();
        }
        progressDialog.show();
        Call<List<JsonNode>> result = RestService.createService(NilaiActivity.this, MainInterface.class).getNilai(kelas);
        result.enqueue(new Callback<List<JsonNode>>() {
            @Override
            public void onResponse(Call<List<JsonNode>> call, Response<List<JsonNode>> response) {
                if (response.isSuccessful()) {
                    try {
                        if (nilaiAdapter == null) {
                            nilaiNode = response.body();
                            nilaiAdapter = new NilaiAdapter(nilaiNode);
                            nilaiAdapter.notifyDataSetChanged();
                            nilaiView.setAdapter(nilaiAdapter);
                        } else {
                            nilaiNode.clear();
                            nilaiAdapter.notifyDataSetChanged();

                            nilaiNode.addAll(response.body());
                            nilaiAdapter.notifyDataSetChanged();
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<JsonNode>> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    public class NilaiAdapter extends RecyclerView.Adapter<NilaiHolder> {
        private List<JsonNode> nilai;

        public NilaiAdapter(List<JsonNode> nilai) {
            this.nilai = nilai;
        }

        @Override
        public NilaiHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(NilaiActivity.this).inflate(R.layout.list_nilai, parent, false);
            return new NilaiHolder(view);
        }

        @Override
        public void onBindViewHolder(NilaiHolder holder, int position) {
            JsonNode node = nilai.get(position);
            holder.bindNilai(node);
        }

        @Override
        public int getItemCount() {
            return nilai.size();
        }
    }

    public class NilaiHolder extends RecyclerView.ViewHolder {
        private TextView mapelText, utsText, uasText, taText;
        private JsonNode nilai;

        public NilaiHolder(View itemView) {
            super(itemView);

            mapelText = (TextView) itemView.findViewById(R.id.txt_mapel);
            utsText = (TextView) itemView.findViewById(R.id.txt_nilai_uts);
            uasText = (TextView) itemView.findViewById(R.id.txt_nilai_uas);
            taText = (TextView) itemView.findViewById(R.id.txt_nilai_ta);
        }

        public void bindNilai(JsonNode node) {
            nilai = node;

            mapelText.setText(getString(R.string.mapel) + " " + node.get("mapel_nama").asText());
            utsText.setText(getString(R.string.nilai_uts) + " " + node.get("nilai_uts").asText());
            uasText.setText(getString(R.string.nilai_uas) + " " + node.get("nilai_uas").asText());
            taText.setText(getString(R.string.nilai_ta) + " " + node.get("nilai_ta").asText());
        }
    }
}
