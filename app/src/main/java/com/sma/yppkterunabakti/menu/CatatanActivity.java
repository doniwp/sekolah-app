package com.sma.yppkterunabakti.menu;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.fasterxml.jackson.databind.JsonNode;
import com.sma.yppkterunabakti.R;
import com.sma.yppkterunabakti.service.MainInterface;
import com.sma.yppkterunabakti.service.RestService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CatatanActivity extends AppCompatActivity {
    private TextView catatanText;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catatan);

        catatanText = (TextView) findViewById(R.id.txt_catatan);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading data...");
        progressDialog.show();

        Call<JsonNode> result = RestService.createService(CatatanActivity.this, MainInterface.class).getCatatan();
        result.enqueue(new Callback<JsonNode>() {
            @Override
            public void onResponse(Call<JsonNode> call, Response<JsonNode> response) {
                if (response.isSuccessful()) {
                    try {
                        JsonNode node = response.body();

                        catatanText.setText(node.get(0).get("siswa_keterangan").asText());
                    } catch (IllegalStateException | NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonNode> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }
}
