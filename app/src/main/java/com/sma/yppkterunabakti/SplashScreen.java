package com.sma.yppkterunabakti;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.sma.yppkterunabakti.service.RestService;
import com.sma.yppkterunabakti.service.SiswaInterface;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {
    private int SPLASH_DISPLAY_LENGTH = 1500;
    private Intent mainIntent;
    private boolean loggedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences preferences = getSharedPreferences(Config.PREFERENCES, MODE_PRIVATE);
                String token = preferences.getString("token", null);
                System.out.println("token " + token);
                loggedIn = token != null && !token.isEmpty();

                if (loggedIn) {
                    mainIntent = new Intent(SplashScreen.this, MainActivity.class);
                    Call<ResponseBody> result = RestService.createService(SplashScreen.this, SiswaInterface.class).refreshToken();
                    result.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (!response.isSuccessful()) {
                                try {
                                    Log.d("message", response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                SharedPreferences preferences = getSharedPreferences(Config.PREFERENCES, MODE_PRIVATE);
                                SharedPreferences.Editor editPref = preferences.edit();
                                editPref.remove("token");
                                editPref.apply();
                            }
                            SplashScreen.this.startActivity(mainIntent);
                            SplashScreen.this.finish();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                            SplashScreen.this.startActivity(mainIntent);
                            SplashScreen.this.finish();
                        }
                    });
                } else {
                    mainIntent = new Intent(SplashScreen.this, LoginActivity.class);
                    SplashScreen.this.startActivity(mainIntent);
                    SplashScreen.this.finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
