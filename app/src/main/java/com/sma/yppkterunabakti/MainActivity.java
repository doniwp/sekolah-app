package com.sma.yppkterunabakti;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sma.yppkterunabakti.menu.AbsensiActivity;
import com.sma.yppkterunabakti.menu.CatatanActivity;
import com.sma.yppkterunabakti.menu.ChangePasswordActivity;
import com.sma.yppkterunabakti.menu.KeuanganActivity;
import com.sma.yppkterunabakti.menu.NilaiActivity;
import com.sma.yppkterunabakti.menu.PelanggaranActivity;
import com.sma.yppkterunabakti.service.FontManager;
import com.sma.yppkterunabakti.service.MainInterface;
import com.sma.yppkterunabakti.service.RestService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private LinearLayout absensiLayout, pelanggaranLayout, catatanLayout, keuanganLayout, nilaiLayout, changeLayout, logoutLayout;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Typeface iconFont = FontManager.getTypeface(getApplicationContext(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(findViewById(R.id.layout_main), iconFont);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading data...");

        absensiLayout = (LinearLayout) findViewById(R.id.btn_absensi);
        pelanggaranLayout = (LinearLayout) findViewById(R.id.btn_pelanggaran);
        catatanLayout = (LinearLayout) findViewById(R.id.btn_catatan);
        keuanganLayout = (LinearLayout) findViewById(R.id.btn_keuangan);
        nilaiLayout = (LinearLayout) findViewById(R.id.btn_nilai);
        changeLayout = (LinearLayout) findViewById(R.id.btn_ganti);
        logoutLayout = (LinearLayout) findViewById(R.id.btn_logout);

        absensiLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AbsensiActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });

        pelanggaranLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PelanggaranActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });

        catatanLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CatatanActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });

        keuanganLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, KeuanganActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });

        nilaiLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NilaiActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });

        changeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ChangePasswordActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });

        logoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void logout() {
        progressDialog.show();
        Call<ResponseBody> result = RestService.createService(this, MainInterface.class).logout();
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    SharedPreferences.Editor editPref = getSharedPreferences(Config.PREFERENCES, MODE_PRIVATE).edit();
                    editPref.remove("token");
                    editPref.remove("siswa_id");
                    editPref.apply();

                    Toast.makeText(getApplicationContext(), "Logout successful.", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
