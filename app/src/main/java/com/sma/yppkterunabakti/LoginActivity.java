package com.sma.yppkterunabakti;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sma.yppkterunabakti.service.MainInterface;
import com.sma.yppkterunabakti.service.RestService;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText usernameText, passwordText;
    private TextView errorView;
    private Button signInButton;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Authenticating...");

        usernameText = (EditText) findViewById(R.id.txt_username);
        passwordText = (EditText) findViewById(R.id.txt_password);
        errorView = (TextView) findViewById(R.id.txt_login_error);
        signInButton = (Button) findViewById(R.id.btn_signin);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernameText.setError(null);
                passwordText.setError(null);

                String username = usernameText.getText().toString();
                String password = passwordText.getText().toString();

                boolean error = false;
                View focusView = null;

                //validate username
                if (TextUtils.isEmpty(username)) {
                    usernameText.setError(getString(R.string.error_field_required));
                    focusView = usernameText;
                    error = true;
                }

                //validate password
                if (TextUtils.isEmpty(password)) {
                    passwordText.setError(getString(R.string.error_field_required));
                    focusView = passwordText;
                    error = true;
                }

                if (error) {
                    focusView.requestFocus();
                } else {
                    loginSubmit(username, password);
                }
            }
        });
    }

    private void loginSubmit(String username, String password) {
        progressDialog.show();
        Call<ResponseBody> result = RestService.createService(getApplicationContext(), MainInterface.class).login(username, password);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    System.out.println("response login " + response.message());
                    if (response.isSuccessful()) {
                        String res = response.body().string();
                        JsonNode json = new ObjectMapper().readTree(res);

                        SharedPreferences.Editor editor = getSharedPreferences(Config.PREFERENCES, MODE_PRIVATE).edit();
                        editor.putString("token", json.get("token").asText());
                        editor.putInt("siswa_id", json.get("id").asInt());
                        editor.apply();

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        System.out.println("response fail " + response.errorBody().string());
                        errorView.setVisibility(View.VISIBLE);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("response login " + t.getMessage());
                progressDialog.dismiss();
            }
        });
    }
}
