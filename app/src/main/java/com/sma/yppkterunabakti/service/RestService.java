package com.sma.yppkterunabakti.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.sma.yppkterunabakti.Config;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by doniw on 10/09/2017.
 */

public class RestService {

    private static String API_BASE_URL = Config.SERVER_URL;
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create());

    public static <S> S createService(final Context context, Class<S> interfaceClass) {
        final String token = getToken(context);
        httpClient.interceptors().clear();
        if (token != null && !token.isEmpty()) {
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    // Request customization: add request headers
                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", "Bearer " + token)
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    Response response = chain.proceed(request);

                    String newToken = response.header("Authorization");

                    if (newToken != null && !newToken.isEmpty()) {
                        newToken = newToken.replaceFirst("Bearer ", "");
                        setNewToken(context, newToken);
                    }

                    return response;
                }
            });
        }
        httpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(interfaceClass);
    }

    private static String getToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Config.PREFERENCES, Context.MODE_PRIVATE);
        return prefs.getString("token", null);
    }

    private static void setNewToken(Context context, String token) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Config.PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.putString("token", token);
        editor.apply();
    }
}
