package com.sma.yppkterunabakti.service;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by doniw on 10/09/2017.
 */

public interface MainInterface {

    @FormUrlEncoded
    @POST("login")
    Call<ResponseBody> login(@Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("absensi")
    Call<List<JsonNode>> getAbsensi(@Field("date") String date);

    @FormUrlEncoded
    @POST("pelanggaran")
    Call<List<JsonNode>> getPelanggaran(@Field("date") String date);

    @GET("catatan")
    Call<JsonNode> getCatatan();

    @FormUrlEncoded
    @POST("keuangan")
    Call<JsonNode> getKeuangan(@Field("month") String month, @Field("year") int year);

    @GET("kelas")
    Call<JsonNode> getKelas();

    @FormUrlEncoded
    @POST("nilai")
    Call<List<JsonNode>> getNilai(@Field("kelas") String kelas);

    @FormUrlEncoded
    @POST("change")
    Call<ResponseBody> change(@Field("password") String password);

    @GET("logout")
    Call<ResponseBody> logout();
}
