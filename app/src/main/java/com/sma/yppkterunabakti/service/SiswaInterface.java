package com.sma.yppkterunabakti.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by doniw on 10/09/2017.
 */

public interface SiswaInterface {
    @GET("refresh")
    Call<ResponseBody> refreshToken();
}
